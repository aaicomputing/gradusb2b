import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { OrdersComponent } from './orders/orders.component';
import { GoodsComponent } from './goods/goods.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { BuyersComponent } from './buyers/buyers.component';
import { SettingsComponent } from './settings/settings.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'orders', component: OrdersComponent },
  { path: 'goods', component: GoodsComponent },
  { path: 'suppliers', component: SuppliersComponent },
  { path: 'buyers', component: BuyersComponent },
  { path: 'settings', component: SettingsComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}