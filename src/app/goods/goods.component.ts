import { Component, OnInit } from '@angular/core';
import { Good } from '../good';
import { GOODS } from '../mock-goods';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.css']
})

export class GoodsComponent implements OnInit {
  good = GOODS;
  constructor() { 
  }
  
  ngOnInit() {
  }
}
