export class Good {
    id: number;
    name: string;
    stats:string;
    shopLink:string;
    stock:number;
    dimension:string;
    photo:string;
    weight:number; 
    category:string;
  }